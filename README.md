LEICA CAMERA FOR ALIOTH/ALIOTHIN

To use this camera first of all clone the repo:

        git clone https://gitlab.com/Eidoron1/android_vendor_xiaomi_camera.git -b miui13 vendor/xiaomi/camera

You need the following device change:

	https://github.com/SuperiorOS-Devices/device_xiaomi_alioth/commit/b8efbbe30afe2fe21489ea4322fca275120de964        

For common tree add this:

        https://github.com/SuperiorOS-Devices/device_xiaomi_sm8250-common/commit/5d389bd036634f938a05a02fc59cb84c66f572cf

To tone down camx logspams

	https://github.com/SuperiorOS-Devices/device_xiaomi_sm8250-common/commit/3f86ed882cb40d08033d207f05fece2df3d5af16

Now some sepolicies for mi camera:

        https://github.com/SuperiorOS-Devices/device_xiaomi_sm8250-common/commit/2f312b3c74ba3cbbf35966905cddba40e461eb90

        https://github.com/SuperiorOS-Devices/device_xiaomi_sm8250-common/commit/f7bf096fd499aec0c9e2715f1fe2b9a6a1b5b3b7

It also requires some source side changes:

	https://review.lineageos.org/c/LineageOS/android_frameworks_av/+/351261

	https://review.lineageos.org/c/LineageOS/android_hardware_interfaces/+/349303

	https://review.lineageos.org/c/LineageOS/android_frameworks_base/+/351262

And thats it. Now start building you rom normally.
